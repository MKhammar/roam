# coding: utf-8

import datetime
import os

import numpy as np
import pandas as pd

from config_variables_TRAM import PROCESSUS_TRAM, COLONNES_FINALES, INFOS_TRAM_2016
from config_variables_colonnes import Caisse_prenante_TRAM, Code_caisse_prenante_TRAM, Caisse_cedante_TRAM, \
    Code_caisse_cédante_TRAM, Date_previsionnelle, Date_debut, Date_fin
from config_variables_globales import DATA_DIR, SOURCES_DIR, RESULTATS_DIR, EXCEL_ROAM_CONSOL, ONGLET_ROAM_CONSOL, EXCEL_TRAM, DATE_INIT, Typologie


#####################################################
# SUPRESSION DES DOUBLONS
#####################################################

def identification_doublons(roam_consol):
    # identification des doublons sur les codes analytique concernés. La date prévisionnelle est soit nulle (cas des lignes en typologie 'Mutualisation') soit égale
    # à '2016-01-01 00:00:00' (démarrage de TRAM, /!\ il ne s'agit pas de DATE_INIT qui est la date de démarrage de la base ROAM). En effet, on ne sélectionne pas les lignes
    # démarrant après '2016-01-01 00:00:00' car dans ce cas, mettons avec une DATE_J > '2016-01-01 00:00:00', on peut avoir une mutualisation hors TRAM (typologie 'Mutualisation')
    # jusqu'à DATE_J - 1, et avoir la mutualisation TRAM qui démarre à DATE_J (typologie 'Centres nationaux')
    orga_doublons = roam_consol.loc[((roam_consol['Code analytique']==3400) |\
                    (roam_consol['Code analytique']==8130)) &\
                    ((roam_consol[Date_previsionnelle]==pd.Timestamp('2016-01-01 00:00:00')) |\
                    (roam_consol[Date_previsionnelle].isnull()))].groupby(['Code analytique',
                                                                             'Code organisme prenant',
                                                                             'Code organisme cédant',
                                                                             'Statut',
                                                                             Typologie]).size().reset_index(level=[0,1,2,3,4])
    orga_doublons.rename(columns={0:'count'},inplace=True)

    # le pivot permet de pivoter les lignes relatives aux typologies `Centres Nationaux` et `Mutualisation` en les mettant en colonnes
    orga_doublons_pivot = pd.pivot_table(orga_doublons,
                           values = 'count',
                           index=['Code analytique', 'Code organisme prenant', 'Code organisme cédant', 'Statut'],
                           columns = Typologie).reset_index()
    orga_doublons_pivot = orga_doublons_pivot.loc[~orga_doublons_pivot['Centres Nationaux'].isnull() & ~orga_doublons_pivot['Mutualisation'].isnull()]
    return orga_doublons_pivot


def lignes_a_supprimer_et_rajouter(roam_consol, orga_doublons_pivot):
    lignes_a_supprimer = pd.DataFrame()
    lignes_a_rajouter = pd.DataFrame()
    for orga_doublon in orga_doublons_pivot.iterrows():
        doublon = roam_consol.loc[(roam_consol['Code analytique'] == orga_doublon[1]['Code analytique']) & \
                                  (roam_consol['Code organisme prenant'] == orga_doublon[1]['Code organisme prenant']) & \
                                  (roam_consol['Code organisme cédant'] == orga_doublon[1]['Code organisme cédant']) & \
                                  (roam_consol['Statut'] == orga_doublon[1]['Statut'])]

        # si 'faux orphelin TRAM', il faut garder la ligne en 'Mutualisation' ainsi que la ligne TRAM 'faux orphelin TRAM' (qui n'a pas encore de valorisation)
        if doublon.iloc[1]['jointure_TRAM'] == 'faux orphelin TRAM':
            ligne_a_rajouter = doublon
        else:
            # les valeurs des indicateurs sont agrégées via une somme
            agg_func = {'ETP à déduire': ['sum'],
                        'Charges à déduire': ['sum']}
            # récupération des résultats issus de l'aggrégation
            doublon_agg = doublon.groupby(['Code organisme prenant', 'Code organisme cédant']).agg(agg_func)

            # on conservera *in fine* la ligne relative à la Typologie 'Centres Nationaux' ...
            ligne_a_supprimer = doublon
            ligne_a_rajouter = doublon.loc[doublon[Typologie] == 'Centres Nationaux']

            # ... pour laquelle on remplace la valeur des indicateurs par la valeur agrégée
            ligne_a_rajouter.set_value(ligne_a_rajouter.index[0], 'ETP à déduire',
                                       doublon_agg['ETP à déduire']['sum'])
            ligne_a_rajouter.set_value(ligne_a_rajouter.index[0], 'Charges à déduire',
                                       doublon_agg['Charges à déduire']['sum'])

            # /!\ on ne veut supprimer des lignes que dans cette boucle
            lignes_a_supprimer = lignes_a_supprimer.append(ligne_a_supprimer)

        lignes_a_rajouter = lignes_a_rajouter.append(ligne_a_rajouter)
        lignes_a_rajouter.loc[:,
        'doublon'] = "doublon fusionné (ligne 'Mutualisation' supprimée, ligne 'Centres Natioanaux' conservée)"

    return lignes_a_supprimer, lignes_a_rajouter

def nettoyage_doublons(roam_consol, lignes_a_supprimer, lignes_a_rajouter):
    roam_consol.drop(labels=list(lignes_a_supprimer.index), axis=0, inplace=True)
    roam_consol = roam_consol.append(lignes_a_rajouter)
    return roam_consol


def retraitement_doublons(data_dir=DATA_DIR, resultats_dir=RESULTATS_DIR, excel_roam_consol=EXCEL_ROAM_CONSOL,
                          onglet_roam_consol=ONGLET_ROAM_CONSOL):
    roam_consol = pd.read_excel(io=os.path.join(data_dir,resultats_dir, excel_roam_consol),
                                sheetname=onglet_roam_consol,
                                converters={'Code organisme prenant': str,
                                            'Code organisme cédant': str})

    orga_doublons_pivot = identification_doublons(roam_consol)
    lignes_a_supprimer, lignes_a_rajouter = lignes_a_supprimer_et_rajouter(roam_consol, orga_doublons_pivot)
    roam_consol_nettoye = nettoyage_doublons(roam_consol, lignes_a_supprimer, lignes_a_rajouter)

    return roam_consol_nettoye


#####################################################
# AJOUT DES DATES DE FIN
#####################################################
def recup_date_moins_un(date_value):
    return pd.Timestamp(date_value.to_pydatetime() - datetime.timedelta(1))


def boucle_maj_date_fin(roam_consol, process_tram, infos_tram=INFOS_TRAM_2016, data_dir=DATA_DIR, sources_dir=SOURCES_DIR, excel_tram=EXCEL_TRAM,
                        date_init=DATE_INIT):
    tram_data = pd.read_excel(io=os.path.join(data_dir,sources_dir, excel_tram),
                              sheetname=process_tram,
                              converters={Caisse_prenante_TRAM: str,
                                          Code_caisse_prenante_TRAM: str,
                                          Caisse_cedante_TRAM: str,
                                          Code_caisse_cédante_TRAM: str})

    tram_demarrage_post_date_init = tram_data.loc[tram_data[Date_debut] > pd.Timestamp(date_init)]

    orga_cedant_list = list(
        set(roam_consol.loc[(roam_consol['Code analytique'] == infos_tram[process_tram]['code_ana']) & \
                            (roam_consol['jointure_TRAM'] == 'orphelin ROAM')]['Code organisme cédant']))

    for orga_cedant in orga_cedant_list:
        # boucle if vérifiant s'il existe au moins une ligne TRAM possédant cet organisme cédant pour le processus TRAM analysé
        if tram_demarrage_post_date_init.loc[tram_demarrage_post_date_init[Code_caisse_cédante_TRAM] == orga_cedant].shape[
            0]:
            # récupération des lignes ROAM (index plus précisément) pour lesquelles la colonne 'Date début' sera mise à jour
            indexes_roam_orga_cedant = list(
                roam_consol.loc[(roam_consol['Code analytique'] == infos_tram[process_tram]['code_ana']) & \
                                (roam_consol['jointure_TRAM'] == 'orphelin ROAM') & \
                                (roam_consol['Code organisme cédant'] == orga_cedant)].index)
            roam_consol.set_value(indexes_roam_orga_cedant, Date_fin, recup_date_moins_un(pd.Timestamp(
                tram_demarrage_post_date_init.loc[
                    tram_demarrage_post_date_init[Code_caisse_cédante_TRAM] == orga_cedant, Date_debut].values[0])))
            roam_consol.loc[indexes_roam_orga_cedant, 'MaJ Date fin'] = 'onglet: ' + process_tram + ', ' + excel_tram
            roam_consol.loc[indexes_roam_orga_cedant, 'correspondance(s) Date fin'] += process_tram + ', '

    return roam_consol


def maj_globale_date_fin(roam_consol, processus_tram=PROCESSUS_TRAM, infos_tram=INFOS_TRAM_2016, data_dir=DATA_DIR,
                        sources_dir=SOURCES_DIR, excel_tram=EXCEL_TRAM, date_init=DATE_INIT):
    """
    /!\ important que l'ajout des dates sur 'PFS Assurés' se fasse après 'PFS Employeurs' (les règles d'attribution en cas d'ambiguïté - ce qui est a priori le cas sur
    les orphelins ROAM - donnant la priorité à 'PFS Assurés').
    """

    # obligé de donner un string pour ensuite faire l'opération "+="
    roam_consol.loc[:, 'correspondance(s) Date fin'] = ''

    for process_tram in processus_tram:
        boucle_maj_date_fin(roam_consol=roam_consol,
                            process_tram=process_tram,
                            infos_tram=infos_tram,
                            data_dir=data_dir,
                            sources_dir=sources_dir,
                            excel_tram=excel_tram,
                            date_init=date_init)

    # suite du 'obligé de donner un string pour ensuite faire l'opération "+="'
    roam_consol['correspondance(s) Date fin'] = roam_consol['correspondance(s) Date fin'].replace('', np.nan)

    return roam_consol


if __name__ == '__main__':
    ROAM_consol_nettoye = retraitement_doublons()
    ROAM_consol_nettoye = maj_globale_date_fin(roam_consol=ROAM_consol_nettoye)

    writer = pd.ExcelWriter(os.path.join(DATA_DIR,RESULTATS_DIR, 'ROAM_2016_consolide_nettoye.xlsx'))
    ROAM_consol_nettoye[COLONNES_FINALES].to_excel(excel_writer=writer, sheet_name='ROAM consolidé nettoyé 2016',
                                                   index=False)
    writer.save()
