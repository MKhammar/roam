##########################################
# Import des librairies et variables
##########################################
import os

import pandas as pd

from config_variables_globales import ANNEE, EXCEL_ROAM_CONSOL_NETTOY, ONGLET_ROAM_CONSOL_NETTOY, INDICATEUR_LIST, \
    COLONNES_TABLE_ORGA, COLONNES_TABLE_VALO, DATA_DIR, SOURCES_DIR, RESULTATS_DIR
from config_variables_colonnes import Date_previsionnelle, Date_debut, Date_fin
from utils import conversion_datetime


##########################################
# Création de la table Orga
##########################################


def construction_orga(data_dir=DATA_DIR, resultats_dir=RESULTATS_DIR, excel_roam_consol_nettoy=EXCEL_ROAM_CONSOL_NETTOY, onglet_roam_consol_nettoy=ONGLET_ROAM_CONSOL_NETTOY):
    ROAM_consol = pd.read_excel(io=os.path.join(data_dir, resultats_dir, excel_roam_consol_nettoy),
                                sheetname=onglet_roam_consol_nettoy,
                                converters={'Code organisme prenant': str,
                                            'Code organisme cédant': str})

    # A REPRENDRE POUR NORMALISER LE HASH ORGA EN CODE OPERATION
    #     Orga = pd.merge(pd.DataFrame(ROAM_consol_2016.groupby('Code orga').size().index),
    #                      ROAM_consol_2016[COLONNES_TABLE_ORGA],
    #                      how='left',
    #                      on=['Code orga'])

    # pour une raison pas encore analysée, il existe des doublons (bizarre!)
    Orga = ROAM_consol[COLONNES_TABLE_ORGA].drop_duplicates(inplace=False)

    # conversion des colonnes en format date (car certaines dates sont en format secondes, alors que d'autres sont au format Timestamp)
    for col in [Date_previsionnelle, Date_debut, Date_fin]:
        Orga[col] = Orga[col].apply(lambda cell: conversion_datetime(cell))

    return Orga, ROAM_consol


##########################################
# Création de la table Valo
##########################################


def construction_valo(roam_consol, annee=ANNEE, col_valo=COLONNES_TABLE_VALO):

    # AGGREGATION DES INDICATEUR_LISTEURS PAR HASH AVEC UN GROUPBY
    agg_func = {'ETP à déduire': ['sum'],
                'Charges à déduire': ['sum']}

    Valo = roam_consol[["Code orga", "Code orga annual", "Année"] + INDICATEUR_LIST]         #.groupby("Code orga").agg(agg_func)

    # Mise en forme du dataframe Valo
    #col_Valo = []
    #for num_col in Valo.columns.labels[0]:
    #    col_Valo += [Valo.columns.levels[0][num_col]]
    #Valo.columns = col_Valo
    #Valo.reset_index(drop=False, inplace=True)

    # EXCLUSION DES LIGNES POUR LESQUELLES LA VALO N'EST PAS RENSEIGNEE
    Valo = Valo.loc[~Valo['Charges à déduire'].isnull() | ~Valo['ETP à déduire'].isnull()]
    #Valo.loc[:, 'Année'] = annee

    return Valo     # [col_valo]


if __name__ == '__main__':
    # Export des tables Orga et Valo
    Orga, ROAM_consol_2016 = construction_orga()
    Valo = construction_valo(ROAM_consol_2016)

    writer = pd.ExcelWriter(os.path.join(DATA_DIR,RESULTATS_DIR,'Orga_Valo_2016.xlsx'))
    Orga.to_excel(excel_writer=writer, sheet_name='Orga 2016', index=False)
    Valo.to_excel(excel_writer=writer, sheet_name='Valo 2016', index=False)
    writer.save()
