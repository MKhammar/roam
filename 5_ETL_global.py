
# coding: utf-8

import importlib
import os

import pandas as pd

from config_variables_TRAM import COLONNES_PRE_FINALES, COLONNES_FINALES
from config_variables_globales import DATA_DIR

etl_roam = importlib.import_module(name='1_ETL_ROAM', package=None)
etl_tram = importlib.import_module(name='2_ETL_TRAM', package=None)
retraitement_roam_consol = importlib.import_module(name='3_retraitement_roam_consol', package=None)
scission_orga_valo = importlib.import_module(name='4_scission_Orga_Valo', package=None)


if __name__ == '__main__':
    data = etl_roam.etl_roam()
    writer_1 = pd.ExcelWriter(os.path.join(DATA_DIR, 'ROAM_2016.xlsx'))
    data.to_excel(excel_writer=writer_1, sheet_name='ROAM 2016', index=False)
    writer_1.save()

    roam_data_final = etl_tram.etl_tram()
    writer_2 = pd.ExcelWriter(os.path.join(DATA_DIR, 'ROAM_2016_consolide.xlsx'))
    roam_data_final[COLONNES_PRE_FINALES].to_excel(excel_writer=writer_2, sheet_name='ROAM consolidé 2016', index=False)
    writer_2.save()

    ROAM_consol_nettoye = retraitement_roam_consol.retraitement_doublons()
    ROAM_consol_nettoye = retraitement_roam_consol.maj_globale_date_fin(roam_consol=ROAM_consol_nettoye)
    writer_3 = pd.ExcelWriter(os.path.join(DATA_DIR, 'ROAM_2016_consolide_nettoye.xlsx'))
    ROAM_consol_nettoye[COLONNES_FINALES].to_excel(excel_writer=writer_3, sheet_name='ROAM consolidé nettoyé 2016', index=False)
    writer_3.save()

    Orga, ROAM_consol_2016 = scission_orga_valo.construction_orga()
    Valo = scission_orga_valo.construction_valo(ROAM_consol_2016)
    writer_4 = pd.ExcelWriter(os.path.join(DATA_DIR, 'Orga_Valo_2016.xlsx'))
    Orga.to_excel(excel_writer=writer_4, sheet_name='Orga 2016', index=False)
    Valo.to_excel(excel_writer=writer_4, sheet_name='Valo 2016', index=False)
    writer_4.save()
