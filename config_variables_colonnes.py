# Colonnes du fichier excel ETP

Code_Caisse = 'Code Caisse'
Caisse = 'Caisse'
Region = 'Région'
Processus = 'Processus'
Code_SAS = 'Code SAS'
Typologie = 'Typologie'
Theme_de_mutualisation = 'Thème de mutualisation'
La_mutualisation_est_elle_totale_ou_partielle = 'La mutualisation est-elle totale ou partielle?'
ETP_a_deduire = 'ETP à déduire'
ETP_a_ajouter = 'ETP à ajouter'
Charges_de_personnel_a_deduire = 'Charges de personnel à déduire'
Charges_de_personnel_a_ajouter = 'Charges de personnel à ajouter'
Autres_charges_a_deduire = 'Autres charges à déduire'
Autres_charges_a_ajouter = 'Autres charges à ajouter'
Total_charges_a_deduire = 'Total charges à déduire'
Total_charges_a_ajouter = 'Total charges à ajouter'
Code_Caisse_prenante = 'Code Caisse prenante'
Caisse_prenante = 'Caisse prenante'
Region = 'Région'

# colonnes spécifiques aux déductions hors mutualisations

Code_DI = "Code  DI (96)"
Motifs_de_deduction = 'Motifs de déduction'
Preciser_obligatoirement_le_motif = 'Préciser obligatoirement le motif  '
Code_processus_91_impacte = 'Code processus 91 impacté'
Support_justificatif_de_la_deduction = 'Support justificatif de la déduction'
Co_signataires = 'Co-signataires'
Date_d_effet_du_dernier_accord_ou_avenant = "Date d'effet du dernier accord ou avenant"
Si_connue_date_de_fin_du_dernier_accord_ou_avenant = "Si connue, date de fin du dernier accord ou avenant"
si_connus_ETP_prévus_dans_l_accord_ou_l_avenant = "si connus, ETP prévus dans l'accord ou l'avenant"
ETP_reellement_deduits = 'ETP réellement déduits '
Charges_de_personnel = 'Charges de personnel'
Autres_charges_hors_locations_affranchissements_amortissements	= 'Autres charges, hors locations affranchissements amortissements'
Total_charges_reellement_deduites = "Total charges réellement déduites"

#colonnes spécifiques au fichier TRAM

Caisse_prenante_TRAM = 'Caisse prenante'
Code_caisse_prenante_TRAM = 'Code caisse prenante'
Caisse_cedante_TRAM = 'Caisse cédante'
Code_caisse_cédante_TRAM = 'Code caisse cédante'
Date_previsionnelle = 'Date prévisionnelle'
Date_debut = 'Date début'
Date_fin = 'Date fin'