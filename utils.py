import numpy as np
import pandas as pd

# remplacement des dates nulles (NaT) par des np.nan (sinon problème lors du merge)
def conversion_nat_nan(df):
    df['Date début'] = [np.nan if pd.isnull(d) else d for d in df['Date début']]
    df['Date prévisionnelle'] = [np.nan if pd.isnull(d) else d for d in df['Date prévisionnelle']]
    
# conversion des colonnes en format date (car certaines dates sont en format secondes, alors que d'autres sont au format Timestamp)
def conversion_datetime(cell):
    if type(cell) is str :
        return cell
    else:
        return pd.to_datetime(cell)


# # certaines dates ont perdu leur format dates. Obligé de passer par un apply car pas réussi à appliquer pd.to_datetime à la Series
# ROAM_avec_dates['Date prévisionnelle'] = ROAM_avec_dates['Date prévisionnelle'].apply(lambda row: pd.to_datetime(row))
# ROAM_avec_dates['Date début'] = ROAM_avec_dates['Date réelle'].apply(lambda row: pd.to_datetime(row))