from config_variables_colonnes import Code_Caisse, Caisse, Region, Processus, \
Code_SAS, Typologie, Theme_de_mutualisation, La_mutualisation_est_elle_totale_ou_partielle, ETP_a_deduire, ETP_a_ajouter, \
Charges_de_personnel_a_deduire, Charges_de_personnel_a_ajouter, Autres_charges_a_deduire, Autres_charges_a_ajouter, \
Total_charges_a_deduire, Total_charges_a_ajouter, Code_Caisse_prenante, Caisse_prenante, Region

from config_variables_colonnes import Code_DI, Motifs_de_deduction, Preciser_obligatoirement_le_motif, Code_processus_91_impacte, \
Support_justificatif_de_la_deduction, Co_signataires, Date_d_effet_du_dernier_accord_ou_avenant, \
Si_connue_date_de_fin_du_dernier_accord_ou_avenant, si_connus_ETP_prévus_dans_l_accord_ou_l_avenant, \
ETP_reellement_deduits, Charges_de_personnel, Autres_charges_hors_locations_affranchissements_amortissements, Total_charges_reellement_deduites

from config_variables_colonnes import Caisse_prenante_TRAM, Code_caisse_prenante_TRAM, Caisse_cedante_TRAM, Code_caisse_cédante_TRAM, \
Date_previsionnelle, Date_debut

DATA_DIR = ".."
SOURCES_DIR  = "sources"
RESULTATS_DIR = "resultats"

ANNEE = 2016

# EXCEL_ROAM_BRUT = "Mutualisations 2016 vEY 20170522.xlsx"
EXCEL_ROAM_BRUT = "Mutualisations 2016 vEY 20170608.xlsx"

ONGLET_MUT_CLSQ = 'MUTUALISATION CLASSIQUE'
ONGLET_MUT_PHR = 'PHARE'
ONGLET_MUT_NAT = 'MUTUALISATION CENTRES NATIONAUX'
ONGLET_DEDUC = 'DEDUCTION HORS MUTUALISATION'

EXCEL_ROAM = "ROAM_2016.xlsx"
ONGLET_ROAM = 'ROAM 2016'
EXCEL_TRAM = "TRAM pour l'Informatique 2016 v20170608.xlsx"


DATE_INIT = '2016-01-01 00:00:00'
DATE_FIN = '2020-12-31 23:59:59'

ORGANISATION_LIST = ["Code organisme prenant",
                     "Code organisme cédant",
                     "Statut",
                     "Périmètre",
                     "Code analytique",
                     Code_SAS,
                     'Motif standardisé',
                     "Motif",
                     Typologie]

ORGANISATION_LIST_ANNUAL = ["Code organisme prenant",
                     "Code organisme cédant",
                     "Statut",
                     "Périmètre",
                     "Code analytique",
                     Code_SAS,
                     'Motif standardisé',
                     "Motif",
                     Typologie,
                        "Année",]

INDICATEUR_LIST = ['ETP à déduire',
                   'Charges à déduire']

EXCEL_ROAM_CONSOL = 'ROAM_2016_consolide.xlsx'
ONGLET_ROAM_CONSOL = 'ROAM consolidé 2016'

EXCEL_ROAM_CONSOL_NETTOY = 'ROAM_2016_consolide_nettoye.xlsx'
ONGLET_ROAM_CONSOL_NETTOY = 'ROAM consolidé nettoyé 2016'

EXCEL_ETP_ANA = 'ETP analytiques 2016.xlsx'
ONGLET_ETP_ANA = 'ETP ANA 2016'


COLONNES_TABLE_ORGA = ['Code orga',
                       'Code organisme prenant',
                       'Code organisme cédant',
                       'Statut',
                       'Périmètre',
                       'Code analytique',
                       Code_SAS,
                       'Motif standardisé',
                       'Motif',
                       Typologie,
                       Date_previsionnelle,
                       Date_debut,
                       'Date fin',
                       'join ambigu',
                       'jointure_TRAM',
                       'raccords multiples',
                       'doublon',
                       'correspondance(s) Date fin',
                       'MaJ Date fin',
                       'Année',
                       'Code orga annual']

COLONNES_TABLE_VALO = ['Code orga',
                       'Année',
                       'ETP à déduire',
                       'Charges à déduire',
                       'Code orga annual']