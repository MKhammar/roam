##########################################
# Import des librairies et variables
##########################################
import os

import hashlib

import numpy as np
import pandas as pd

from config_variables_globales import DATA_DIR, SOURCES_DIR, RESULTATS_DIR, EXCEL_TRAM, EXCEL_ROAM, ONGLET_ROAM, ORGANISATION_LIST, ORGANISATION_LIST_ANNUAL, ANNEE
from utils import conversion_nat_nan

from config_variables_colonnes import Code_Caisse, Caisse, Region, Processus, \
Code_SAS, Typologie, Theme_de_mutualisation, La_mutualisation_est_elle_totale_ou_partielle, ETP_a_deduire, ETP_a_ajouter, \
Charges_de_personnel_a_deduire, Charges_de_personnel_a_ajouter, Autres_charges_a_deduire, Autres_charges_a_ajouter, \
Total_charges_a_deduire, Total_charges_a_ajouter, Code_Caisse_prenante, Caisse_prenante, Region

from config_variables_colonnes import Caisse_prenante_TRAM, Code_caisse_prenante_TRAM, Caisse_cedante_TRAM, Code_caisse_cédante_TRAM, \
Date_previsionnelle, Date_debut

from config_variables_TRAM import COL_TRAM, ANNEE_SUIVANTE, HYPOTHESES_DATES, INFOS_TRAM_2016, PROCESSUS_TRAM, COLONNES_ROAM, COLONNES_PRE_FINALES

##########################################
# Chargement des données TRAM
##########################################
# remplacement des valeurs 'Organisme déjà cédant' ou 'Pôle individuel' par des dates hypothétiques
def rplcmt_dates_hypotheses(row, col_prev, col_debut, reel=False):
    if row[col_prev] in HYPOTHESES_DATES.keys():
        return HYPOTHESES_DATES[row[col_prev]]
    elif reel:
        return row[col_debut]
    else:
        return row[col_prev]


# récup des données issues du fichier TRAM pour l'informatique, ajout des colonnes 'Statut' (c'est là qu'on identifie les organismes autonomes),
# 'Code analytique', 'Code SAS', 'Motif standardisé', 'Typologie'  et remplacement des valeurs 'Organisme déjà cédant' ou 'Pôle individuel' dans la colonne 'Date prévisionnelle'
# NB : les données retournées manqueront encore des lignes relatives aux organismes prenants (lignes "symétriques" sur l'organisme prenant avec le statut 'Prenant' ou 'Cédant')
def recup_onglet_tram(processus_tram, data_dir=DATA_DIR, sources_dir = SOURCES_DIR, excel_tram=EXCEL_TRAM, hypotheses_dates=HYPOTHESES_DATES, col_tram=COL_TRAM):
    # chargement des données issues du fichier TRAM pour l'informatique
    tram_data = pd.read_excel(io=os.path.join(data_dir, sources_dir, excel_tram),
                              sheetname=processus_tram,
                              converters={Caisse_prenante_TRAM : str,
                                          Code_caisse_prenante_TRAM : str,
                                          Caisse_cedante_TRAM : str,
                                          Code_caisse_cédante_TRAM : str})

    # renommage des colonnes
    tram_data.rename(index=str,
                columns={Code_caisse_prenante_TRAM: 'Code organisme prenant',
                         Caisse_prenante_TRAM: 'Organisme prenant',
                         Code_caisse_cédante_TRAM: 'Code organisme cédant',
                         Caisse_cedante_TRAM: 'Organisme cédant',
                         'Date réelle': 'Date début'},                              #inutile ??
                inplace=True)

    tram_data.loc[:, 'Statut'] = 'Cédant'
    tram_data.loc[:, 'Code analytique'] = INFOS_TRAM_2016[processus_tram]['code_ana']
    tram_data.loc[:, 'Code SAS'] = INFOS_TRAM_2016[processus_tram]['code_sas']
    tram_data.loc[:, 'Motif standardisé'] = INFOS_TRAM_2016[processus_tram]['motif standardise']
    tram_data.loc[:, 'Typologie'] = INFOS_TRAM_2016[processus_tram]['typo']

    # mise à jour de la colonne 'Statut' sur les organismes autonomes
    if tram_data[Date_previsionnelle].dtype.name == 'object':
        # if pour contrôler qu'il existe des lignes 'Pôle individuel'
        if (tram_data[Date_previsionnelle] == 'Pôle individuel').sum():
            tram_data.loc[tram_data[Date_previsionnelle] == 'Pôle individuel', 'Statut'] = 'Autonome'

    # remplacement des valeurs 'Organisme déjà cédant' ou 'Pôle individuel' par des dates hypothétiques
    # /!\ il faut d'abord commencer par 'Date début'
    tram_data.loc[:, Date_debut] = tram_data[[Date_previsionnelle, Date_debut]].apply(lambda row: rplcmt_dates_hypotheses(row,
                                                                                                                                  col_prev=Date_previsionnelle,
                                                                                                                                  col_debut=Date_debut,
                                                                                                                                  reel=True),
                                                                                              axis=1)
    tram_data.loc[:, Date_previsionnelle] = tram_data[[Date_previsionnelle, Date_debut]].apply(lambda row: rplcmt_dates_hypotheses(row,
                                                                                                                                          col_prev=Date_previsionnelle,
                                                                                                                                         col_debut=Date_debut,
                                                                                                                                          reel=False),
                                                                                                      axis=1)
    return tram_data[col_tram]


# création des lignes relatives aux organismes prenants (pas natives dans le fichier TRAM pour l'informatique)
def ajout_prenant(tram_data, col_tram=COL_TRAM):
    # fonction de calcul de la date minimale sur les deux colonnes 'Date prévisionnelle' et 'Date début', utilisées par hypothèse pour l'organisme prenant
    def min_dates(df):
        return pd.DataFrame({Date_previsionnelle: [np.min(pd.to_datetime(df[Date_previsionnelle]))],
                             Date_debut: [np.min(pd.to_datetime(df[Date_debut]))]})

    # calcul de la date minimale par organisme prenant
    orga_prenant = tram_data.loc[tram_data['Statut'] == 'Cédant'].groupby(['Code organisme prenant', 'Organisme prenant']).apply(min_dates)
    orga_prenant['Code organisme prenant'] = orga_prenant.index.get_level_values('Code organisme prenant')

    # récup des valeurs du processus TRAM considéré sur les colonnes 'Code analytique', 'Code SAS', 'Motif standardisé' et 'Typologie'
    ligne_example = tram_data.loc['0']

    # création des nouvelles lignes par type_statut ('Prenant' ou 'Cédant')
    def creer_df_prenant(type_statut):
        df_len = orga_prenant.shape[0]
        return pd.DataFrame({'Code organisme prenant': orga_prenant.index.get_level_values('Code organisme prenant'),
                             'Organisme prenant': orga_prenant.index.get_level_values('Organisme prenant'),
                             'Code organisme cédant': orga_prenant.index.get_level_values('Code organisme prenant'),
                             'Organisme cédant': orga_prenant.index.get_level_values('Organisme prenant'),
                             Date_previsionnelle: orga_prenant[Date_previsionnelle].values,
                             Date_debut: orga_prenant[Date_debut].values,
                             'Statut': [type_statut] * df_len,
                             'Code analytique': [ligne_example['Code analytique']] * df_len,
                             Code_SAS: [ligne_example[Code_SAS]] * df_len,
                             'Motif standardisé': [ligne_example['Motif standardisé']] * df_len,
                             Typologie: [ligne_example[Typologie]] * df_len})

    return pd.concat([creer_df_prenant('Prenant'),
                      creer_df_prenant('Cédant')],
                     axis=0)[col_tram]


# récup des données TRAM retraitées par onglet
def recup_total_tram_data(processus_tram, data_dir=DATA_DIR, sources_dir = SOURCES_DIR, excel_tram=EXCEL_TRAM, col_tram=COL_TRAM):
    lignes_prenantes = ajout_prenant(recup_onglet_tram(processus_tram, data_dir, sources_dir, excel_tram))
    tram_data = recup_onglet_tram(processus_tram, data_dir, sources_dir, excel_tram)
    return pd.concat([lignes_prenantes, tram_data], axis=0)[col_tram].sort_values(['Code organisme prenant', 'Statut', 'Code organisme cédant', Date_previsionnelle],
                                                                                  ascending=[True, False, True, True])


##########################################################################
# Jointure des déclarations ROAM avec les données TRAM pour l'informatique
##########################################################################

# raccord de l'organisation et des dates TRAM aux lignes *a priori* correspondantes dans les déclarations ROAM
# cette jointure est faite par processus TRAM (cette fonction sera donc utilisée de manière itérative sur les différents processus)
# NB : *a priori* car on sélectionne toutes les lignes dans les déclarations ROAM possédant la même typologie et le même code analytique
# mais certaines lignes - qualifiées d'orphelin ROAM - ne correspondent pas à du TRAM : leur typologie est changée à 'Mutualisation'
def raccord_ROAM_TRAM(processus_tram, data_dir=DATA_DIR, sources_dir=SOURCES_DIR, resultats_dir=RESULTATS_DIR, excel_roam=EXCEL_ROAM, onglet_roam=ONGLET_ROAM, excel_tram=EXCEL_TRAM):
    roam_data = pd.read_excel(io=os.path.join(data_dir, resultats_dir, excel_roam),
                              sheetname=onglet_roam,
                              converters={'Code organisme prenant': str,
                                          'Code organisme cédant': str})

    tram_data = recup_total_tram_data(processus_tram, data_dir, sources_dir, excel_tram)

    # /!\ IMPORTANT !
    # Dans les déclarations ROAM, il ne faut sélectionner que les lignes relatives au même code analytique mais aussi celles pré-typées avec la même typologie
    # En effet, il existe des lignes avec le Code analytique 5120 ('Acceuil téléphonique') dans la typologie 'Centres Nationaux' (correspond au PFS Délestage : il ne faut pas les sélectionner)
    # De même, il existe aussi des lignes avec le Code analytique 5120 ('Acceuil téléphonique') et 2115 ('FSP LAD') dans la typologie (PHARE) (correspond à du PHARE : idem, il ne faut pas les sélectionner)
    select_index = ((roam_data['Code analytique'] == INFOS_TRAM_2016[processus_tram]['code_ana'])
                    & (roam_data[Typologie] == INFOS_TRAM_2016[processus_tram]['typo']))

    # /!\ 'Périmètre' et 'Motif' ne sont pas pris en compte pour la jointure (information absente des données TRAM pour l'informatique)
    roam_dates_data = pd.merge(roam_data.loc[select_index],
                               tram_data,
                               how='outer',
                               on=['Code organisme prenant',
                                   'Code organisme cédant',
                                   'Statut',
                                   'Code analytique',
                                   Code_SAS,
                                   Typologie])

    # AJOUT DE LA COLONNE 'jointure_TRAM' :
    # 1-initialisation à 'OK'
    roam_dates_data.loc[:, 'jointure_TRAM'] = pd.Series(['OK'] * roam_dates_data.shape[0], index=roam_dates_data.index)
    # 2-remplacement des valeurs pour les orphelins ROAM
    roam_dates_data.loc[roam_dates_data[Date_previsionnelle].isnull(), 'jointure_TRAM'] = pd.Series(['orphelin ROAM'] * roam_dates_data[Date_previsionnelle].isnull().sum(),
                                                                                                      index=roam_dates_data.loc[roam_dates_data[Date_previsionnelle].isnull()].index)
    # 3-remplacement des valeurs pour les VRAIS orphelins TRAM
    orph_tram = ((roam_dates_data['Statut'] != 'Autonome') & roam_dates_data['Charges à déduire'].isnull() & (
    pd.to_datetime(roam_dates_data[Date_previsionnelle]) < pd.to_datetime(ANNEE_SUIVANTE)))
    roam_dates_data.loc[orph_tram, 'jointure_TRAM'] = pd.Series(['orphelin TRAM'] * roam_dates_data.loc[orph_tram].shape[0],
                                                                index=roam_dates_data.loc[orph_tram].index)
    # 4-remplacement des valeurs pour les FAUX orphelins TRAM (organismes autonomes et opération portant sur l'année suivante)
    faux_orph_tram = ((roam_dates_data['Charges à déduire'].isnull()) & (
    (roam_dates_data['Statut'] == 'Autonome') | (pd.to_datetime(roam_dates_data[Date_previsionnelle]) >= pd.to_datetime(ANNEE_SUIVANTE))))
    roam_dates_data.loc[faux_orph_tram, 'jointure_TRAM'] = pd.Series(['faux orphelin TRAM'] * roam_dates_data.loc[faux_orph_tram].shape[0],
                                                                     index=roam_dates_data.loc[faux_orph_tram].index)

    # suppression des libellés organisme issus des déclarations ROAM
    roam_dates_data.drop(['Organisme prenant_x', 'Organisme cédant_x'], axis=1, inplace=True)
    # renommage des libellés organisme issus de TRAM pour l'informatique
    roam_dates_data.rename(columns={'Organisme prenant_y': 'Organisme prenant (TRAM)',
                                    'Organisme cédant_y': 'Organisme cédant (TRAM)'},
                           inplace=True)

    return roam_dates_data

# identification et comptage des lignes pour lesquels il y a plusieurs raccordements TRAM aux déclarations ROAM
def analyse_raccords_multiples(roam_dates_data):
    # sélection des lignes TRAM raccordées
    lignes_TRAM = roam_dates_data['jointure_TRAM'].apply(lambda cell_value: cell_value in ['OK'])
    # comptage du nombre de fois où elles apparaissent
    # pas mis les colonnes 'Code analytique' et 'Typologie' car toutes les valeurs sont identiques au sein du processus étudié
    # /!\ pas mis la colonne 'Code SAS' car le groupby n'aime pas les np.nan mais il faudra peut-être les mettre à l'avenir (quand ils seront renseignés)
    count_raccords = roam_dates_data.loc[lignes_TRAM,].groupby(['Code organisme prenant',
                                                                'Code organisme cédant',
                                                                'Statut']).size()

    # comptage du nombre de lignes en excès
    raccords_exces = (count_raccords.loc[count_raccords > 1] - 1).sum()

    # création d'un dataframe contenant les infos sur les raccords multiples
    dict_orga_raccords_multiples = {}
    for col_name in count_raccords.index.names:
        dict_orga_raccords_multiples[col_name] = list(count_raccords.loc[count_raccords > 1].index.get_level_values(col_name))
    dict_orga_raccords_multiples['raccords multiples'] = count_raccords.loc[count_raccords > 1].values
    raccords_multiples = pd.DataFrame(dict_orga_raccords_multiples)

    return raccords_multiples, raccords_exces


# jointure entre les déclarations roam et tram, avec identification des lignes 'en excès'
def raccord_ROAM_TRAM_avec_analyse(processus_tram, data_dir=DATA_DIR, sources_dir=SOURCES_DIR, resultats_dir=RESULTATS_DIR, excel_roam=EXCEL_ROAM, onglet_roam=ONGLET_ROAM, excel_tram=EXCEL_TRAM):
    roam_dates_data = raccord_ROAM_TRAM(processus_tram, data_dir=data_dir, sources_dir=sources_dir, resultats_dir=resultats_dir, excel_roam=excel_roam, onglet_roam=onglet_roam, excel_tram=excel_tram)
    raccords_multiples, _ = analyse_raccords_multiples(roam_dates_data)
    roam_dates_data_analyse = pd.merge(roam_dates_data, raccords_multiples, how='outer', on=['Code organisme prenant', 'Code organisme cédant', 'Statut'])
    return roam_dates_data_analyse


# analyse - comptabilisation des lignes entre les tableaux sources et le tableau en sortie (jointure de ROAM et de TRAM)
def comptage_total_lignes_ROAM_lignes_TRAM(processus_tram=PROCESSUS_TRAM, data_dir=DATA_DIR, sources_dir=SOURCES_DIR, excel_roam=EXCEL_ROAM, onglet_roam=ONGLET_ROAM, excel_tram=EXCEL_TRAM):
    def comptage_lignes_ROAM_lignes_TRAM(process_tram, select_index, data_dir, sources_dir, resultats_dir, excel_roam, onglet_roam, excel_tram):
        roam_dates_data_analyse = raccord_ROAM_TRAM_avec_analyse(process_tram, data_dir, sources_dir, resultats_dir, excel_roam, onglet_roam, excel_tram)

        lignes_ok_hors_doublon = roam_dates_data_analyse.loc[roam_dates_data_analyse['jointure_TRAM'] == 'OK'].groupby(['Code organisme prenant', 'Code organisme cédant', 'Statut']).size().shape[
            0]
        lignes_orph = roam_dates_data_analyse.groupby('jointure_TRAM').size().loc[['orphelin TRAM', 'faux orphelin TRAM']].sum()

        comptage_lignes = pd.DataFrame({'Processus TRAM': [process_tram],
                                        'lignes ROAM récupérées (post jointure)': [roam_dates_data_analyse.groupby('jointure_TRAM').size().loc[['OK', 'orphelin ROAM']].sum()],
                                        'lignes ROAM correspondantes (fichier original)': [roam_data.loc[select_index].shape[0]],
                                        'lignes TRAM récupérées (net de doublons ; post jointure)': [lignes_ok_hors_doublon + lignes_orph],
                                        'lignes TRAM correspondantes (fichier original)': [recup_total_tram_data(process_tram, data_dir, excel_tram).shape[0]]
                                        })
        return comptage_lignes

    roam_data = pd.read_excel(io=os.path.join(data_dir, sources_dir, excel_roam),
                              sheetname='ROAM 2016',
                              converters={'Code organisme prenant': str,
                                          'Code organisme cédant': str})

    comptage = pd.DataFrame()
    for process_tram in processus_tram:
        select_index = (roam_data['Code analytique'] == INFOS_TRAM_2016[process_tram]['code_ana']) & (roam_data['Typologie'] == INFOS_TRAM_2016[process_tram]['typo'])
        comptage = comptage.append(comptage_lignes_ROAM_lignes_TRAM(process_tram, select_index, data_dir, excel_roam, onglet_roam, excel_tram))
    return comptage


# analyse - lignes en commun (intersection) entre les données roam_tram (données ROAM jointes avec les données TRAM pour l'informatique) pour chaque processus TRAM
def analyse_intersection_roam_tram(processus_tram=PROCESSUS_TRAM, data_dir=DATA_DIR, sources_dir=SOURCES_DIR, resultats_dir=RESULTATS_DIR, excel_roam=EXCEL_ROAM, onglet_roam=ONGLET_ROAM, excel_tram=EXCEL_TRAM):
    intersect_df = pd.DataFrame(index=processus_tram)
    for i, process_tram_1 in enumerate(processus_tram[:]):
        roam_tram_process_1 = raccord_ROAM_TRAM_avec_analyse(process_tram_1, data_dir, sources_dir, resultats_dir, excel_roam, onglet_roam, excel_tram)
        for process_tram_2 in processus_tram[i + 1:]:
            roam_tram_process_2 = raccord_ROAM_TRAM_avec_analyse(process_tram_2, data_dir, sources_dir, resultats_dir, excel_roam, onglet_roam, excel_tram)
            intersect_df.loc[process_tram_1, process_tram_2] = pd.merge(roam_tram_process_1,
                                                                        roam_tram_process_2,
                                                                        how='inner',
                                                                        on=['Code organisme prenant',
                                                                            'Code organisme cédant',
                                                                            'Statut',
                                                                            'Périmètre',
                                                                            'Code analytique',
                                                                            Code_SAS,
                                                                            'Motif',
                                                                            Typologie,
                                                                            'ETP à déduire',
                                                                            'Charges à déduire']).shape[0]
    return intersect_df

# obtention de la base consolidée des déclarations ROAM avec les données TRAM pour l'informatique, en prenant soin d'enlever les doublons
def join_roam_tram_consolide(processus_tram=PROCESSUS_TRAM, data_dir=DATA_DIR, sources_dir=SOURCES_DIR, resultats_dir=RESULTATS_DIR, excel_roam=EXCEL_ROAM, onglet_roam=ONGLET_ROAM, excel_tram=EXCEL_TRAM, colonnes_roam=COLONNES_ROAM):
    # création d'une table regroupant toutes les données roam et tram (y compris doublons)
    roam_dates_analyse_total_brut = pd.DataFrame()
    for process_tram in processus_tram :
        roam_dates_analyse = raccord_ROAM_TRAM_avec_analyse(process_tram, data_dir=data_dir, sources_dir = sources_dir, resultats_dir= resultats_dir, excel_roam=excel_roam, onglet_roam=onglet_roam, excel_tram=excel_tram)
        roam_dates_analyse_total_brut = roam_dates_analyse_total_brut.append(roam_dates_analyse)


    # DEBUT DU TRAVAIL SUR LES DOUBLONS ('PFS Assurés' et 'PFS Employeurs')
    # re-définition d'un index incrémental et conservation dans une colonne (car l'index des tables disparait avec un merge)
    roam_dates_analyse_total_brut.reset_index(drop=True, inplace=True)
    roam_dates_analyse_total_brut_bis = roam_dates_analyse_total_brut.reset_index(drop=False, inplace=False)

    roam_tram_PFS_Ass = raccord_ROAM_TRAM_avec_analyse('PFS Assurés', data_dir, sources_dir, resultats_dir, excel_roam, onglet_roam, excel_tram)
    roam_tram_PFS_emp = raccord_ROAM_TRAM_avec_analyse('PFS Employeurs', data_dir,sources_dir, resultats_dir, excel_roam, onglet_roam, excel_tram)
    intersect_PFS_Ass_Emp = pd.merge(roam_tram_PFS_Ass,
                                     roam_tram_PFS_emp,
                                     how='inner',
                                     on=colonnes_roam)

    # identifications des lignes 'PFS Assurés' et 'PFS Employeurs' en doublons
    doublons_roam_tram = pd.merge(roam_dates_analyse_total_brut_bis,
                                 intersect_PFS_Ass_Emp,
                                 how='inner',
                                 on = colonnes_roam)
    index_doublons = doublons_roam_tram.groupby('index').size().index


    # suppression de toutes les lignes 'PFS Assurés' et 'PFS Employeurs' en doublons
    roam_dates_analyse_total_final = roam_dates_analyse_total_brut.drop(labels=index_doublons, inplace=False)

    # on identifie les colonnes se terminant par le suffixe '_x'
    _x_cols = []
    for column, keep_col in zip(list(intersect_PFS_Ass_Emp.columns), ['_x' in column for column in list(intersect_PFS_Ass_Emp.columns)]):
        if keep_col:
            _x_cols += [column]
    # on identifie les colonnes se terminant par le suffixe '_y'
    _y_cols = []
    for column, keep_col in zip(list(intersect_PFS_Ass_Emp.columns), ['_y' in column for column in list(intersect_PFS_Ass_Emp.columns)]):
        if keep_col:
            _y_cols += [column]

    # on garde toutes les colonnes sauf les deux dernières
    tram_final_cols = []
    for _x_col in _x_cols:
        tram_final_cols += [_x_col[:-2]]

    # par défaut, sur les colonne finales, on garde toutes les valeurs contenues dans 'PFS Assurés'
    for final_col, _x_col in zip(tram_final_cols, _x_cols):
        intersect_PFS_Ass_Emp.loc[:, final_col] = intersect_PFS_Ass_Emp[_x_col]

    index_priorite_emp = intersect_PFS_Ass_Emp.loc[(intersect_PFS_Ass_Emp['jointure_TRAM_x']=='orphelin ROAM') &\
                             (intersect_PFS_Ass_Emp['jointure_TRAM_y']=='OK')].index

    for final_col, _y_col in zip(tram_final_cols, _y_cols):
        intersect_PFS_Ass_Emp.loc[index_priorite_emp, final_col] = intersect_PFS_Ass_Emp.loc[index_priorite_emp, _y_col]

    intersect_PFS_Ass_Emp.loc[:, 'join ambigu'] = 'PFS Assurés > PFS Employeurs'
    intersect_PFS_Ass_Emp.loc[index_priorite_emp, 'join ambigu'] = 'PFS Employeurs > PFS Assurés'

    # # vérification de la bonne affectation 'PFS Assurés' ou 'PFS Employeurs':
    # intersect_PFS_Ass_Emp.loc[:, ['jointure_TRAM_x', 'jointure_TRAM_y', 'jointure_TRAM', 'join ambigu']].groupby(['join ambigu', 'jointure_TRAM_x', 'jointure_TRAM_y', 'jointure_TRAM']).size()

    # rajout des lignes (cette fois sans doublons)
    roam_dates_analyse_total_final = roam_dates_analyse_total_final.append(intersect_PFS_Ass_Emp[colonnes_roam+tram_final_cols+['join ambigu']])
    # FIN DU TRAVAIL SUR LES DOUBLONS ('PFS Assurés' et 'PFS Employeurs')

#     print('check (doit être 0):', intersect_PFS_Ass_Emp.shape[0] - (roam_dates_analyse_total_brut.shape[0] - roam_dates_analyse_total_final.shape[0]))

    return roam_dates_analyse_total_final


# join final entre les déclarations ROAM et la base consolidée des déclarations ROAM avec les données TRAM pour l'informatique
def join_final_roam_tram(processus_tram=PROCESSUS_TRAM, data_dir=DATA_DIR, sources_dir=SOURCES_DIR, resultats_dir=RESULTATS_DIR,excel_roam=EXCEL_ROAM, onglet_roam=ONGLET_ROAM, excel_tram=EXCEL_TRAM, colonnes_roam=COLONNES_ROAM):
    roam_data = pd.read_excel(io=os.path.join(data_dir, resultats_dir, excel_roam),
                              sheetname=onglet_roam,
                              converters={'Code organisme prenant': str,
                                          'Code organisme cédant': str})
    roam_dates_analyse_total_final = join_roam_tram_consolide(processus_tram, data_dir,sources_dir, resultats_dir, excel_roam, onglet_roam, excel_tram, colonnes_roam)
    roam_data_final = pd.merge(roam_data,
                               roam_dates_analyse_total_final,
                               how='outer',
                               on=colonnes_roam)

    #     print('check (doit être 0):', (roam_data_final.shape[0] - roam_data.shape[0]) - roam_data_final.loc[(roam_data_final['jointure_TRAM']=='orphelin TRAM') | (roam_data_final['jointure_TRAM']=='faux orphelin TRAM')].shape[0])

    return roam_data_final


def completer_motif_standardise(roam_data_final):
    roam_data_final.loc[roam_data_final['Motif standardisé'].isnull(), 'Motif standardisé'] = roam_data_final.loc[roam_data_final['Motif standardisé'].isnull(), 'Motif']
    return roam_data_final


def correction_typologies(roam_data_final):
    roam_data_final.loc[(roam_data_final['jointure_TRAM'] == 'orphelin ROAM') & (roam_data_final[Typologie] == 'Pôle Régionaux'), Typologie] = 'Mutualisation'
    return roam_data_final

# fonction calculant un hash à partir d'une liste de colonnes
# dans un premier temps, le contenu des colonnes est concaténé, puis dans un second temps le hash correspondant est calculé
def ajout_code_orga(df, orga_list):
    def concat_cols(pd_serie, col_list):
        return "".join([str(item) for item in pd_serie[col_list]]).encode(encoding="UTF-8")
    df.loc[:,'Code orga'] = df.replace(np.nan,'').apply(func=lambda row: hashlib.md5(concat_cols(row, orga_list)).hexdigest(), axis=1)

def ajout_code_orga_annual(df, orga_list):
    def concat_cols(pd_serie, col_list):
        return "".join([str(item) for item in pd_serie[col_list]]).encode(encoding="UTF-8")
    df.loc[:,'Code orga annual'] = df.replace(np.nan,'').apply(func=lambda row: hashlib.md5(concat_cols(row, orga_list)).hexdigest(), axis=1)


def etl_tram(processus_tram=PROCESSUS_TRAM, data_dir=DATA_DIR,sources_dir=SOURCES_DIR, resultats_dir=RESULTATS_DIR, excel_roam=EXCEL_ROAM, onglet_roam=ONGLET_ROAM, excel_tram=EXCEL_TRAM, colonnes_roam=COLONNES_ROAM, orga_list=ORGANISATION_LIST, orga_list_annual = ORGANISATION_LIST_ANNUAL):
    roam_data_final = join_final_roam_tram(processus_tram, data_dir,sources_dir, resultats_dir, excel_roam, onglet_roam, excel_tram, colonnes_roam)
    roam_data_final = completer_motif_standardise(roam_data_final)
    roam_data_final = correction_typologies(roam_data_final)
    conversion_nat_nan(roam_data_final)
    # ajout du hash (= code organisation)
    ajout_code_orga(roam_data_final, orga_list)
    roam_data_final["Année"] = ANNEE

    ajout_code_orga_annual(roam_data_final, orga_list_annual)

    return roam_data_final


if __name__ == '__main__':

    roam_data_final = etl_tram()
    writer = pd.ExcelWriter(os.path.join(DATA_DIR, RESULTATS_DIR, 'ROAM_2016_consolide.xlsx'))
    roam_data_final[COLONNES_PRE_FINALES].to_excel(excel_writer=writer, sheet_name='ROAM consolidé 2016', index=False)
    writer.save()
