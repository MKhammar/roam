##########################################
# Import des librairies et variables
##########################################
import os
import sklearn

import numpy as np
import pandas as pd

from config_variables_globales import DATA_DIR, SOURCES_DIR, RESULTATS_DIR, EXCEL_ROAM_BRUT, ONGLET_MUT_CLSQ, ONGLET_MUT_PHR, ONGLET_MUT_NAT, ONGLET_DEDUC

from config_variables_colonnes import Code_Caisse, Caisse, Region, Processus, \
Code_SAS, Typologie, Theme_de_mutualisation, La_mutualisation_est_elle_totale_ou_partielle, ETP_a_deduire, ETP_a_ajouter, \
Charges_de_personnel_a_deduire, Charges_de_personnel_a_ajouter, Autres_charges_a_deduire, Autres_charges_a_ajouter, \
Total_charges_a_deduire, Total_charges_a_ajouter, Code_Caisse_prenante, Caisse_prenante, Region

from config_variables_colonnes import Code_DI, Motifs_de_deduction, Preciser_obligatoirement_le_motif, Code_processus_91_impacte, \
Support_justificatif_de_la_deduction, Co_signataires, Date_d_effet_du_dernier_accord_ou_avenant, \
Si_connue_date_de_fin_du_dernier_accord_ou_avenant, si_connus_ETP_prévus_dans_l_accord_ou_l_avenant, \
ETP_reellement_deduits, Charges_de_personnel, Autres_charges_hors_locations_affranchissements_amortissements, Total_charges_reellement_deduites


##########################################
# Chargement des déclarations ROAM
##########################################
# chargement des données relatives aux déductions hors mutualisation
# chargement des données relatives aux déductions hors mutualisation
def read_xls_deduc_tab(onglet_deduc, data_dir=DATA_DIR, sources_dir = SOURCES_DIR, excel_roam_brut=EXCEL_ROAM_BRUT):
    deduc_data = pd.read_excel(io=os.path.join(data_dir, sources_dir, excel_roam_brut),
                               sheetname=onglet_deduc,
                               converters={Code_Caisse : str,
                                           Caisse : str,
                                           Region : str,
                                           Code_DI : str,
                                           Code_SAS : str,
                                           Typologie : str,
                                           Preciser_obligatoirement_le_motif : str})

    # renommage des colonnes
    deduc_data.rename(index=str,
                      columns={Code_Caisse : Code_Caisse_prenante,
                               Caisse : Caisse_prenante,
                               Code_DI : Processus,
                               Preciser_obligatoirement_le_motif : Theme_de_mutualisation,
                               ETP_reellement_deduits : ETP_a_deduire,
                               Total_charges_reellement_deduites : Total_charges_a_deduire},
                      inplace=True)

    # rajout des colonnes manquantes (NaN pour les vides, "Pas une mutualisation" pour le périmètre)
    nan_list = [np.nan] * deduc_data.shape[0]
    df_empty_columns = pd.DataFrame({Code_Caisse: nan_list,
                                     Caisse : nan_list,
                                     ETP_a_ajouter: nan_list,
                                     Total_charges_a_ajouter : nan_list,
                                     La_mutualisation_est_elle_totale_ou_partielle : ['Pas une mutualisation'] * deduc_data.shape[0]})
    df_empty_columns.index = deduc_data.index
    deduc_data = pd.concat([deduc_data, df_empty_columns], axis=1)

    return deduc_data[[Code_Caisse_prenante,
                       Caisse_prenante,
                       Code_Caisse,
                       Caisse,
                       La_mutualisation_est_elle_totale_ou_partielle,
                       Processus,
                       Code_SAS,
                       Theme_de_mutualisation,
                       Typologie,
                       ETP_a_ajouter,
                       ETP_a_deduire,
                       Total_charges_a_ajouter,
                       Total_charges_a_deduire]]


# chargement des données relatives aux mutualisations ("mutualisations classiques", "centres nationaux" et PHARE")
def read_xls_mut_tab(onglet_mut, data_dir=DATA_DIR, sources_dir = SOURCES_DIR, excel_roam_brut=EXCEL_ROAM_BRUT):
    mut_data = pd.read_excel(io=os.path.join(data_dir, sources_dir, excel_roam_brut),
                             sheetname=onglet_mut,
                             converters={Code_Caisse: str,
                                         Caisse: str,
                                         Region : str,
                                         Processus : str,
                                         Code_SAS: str,
                                         Typologie : str,
                                         Theme_de_mutualisation : str,
                                         La_mutualisation_est_elle_totale_ou_partielle : str,
                                         Code_Caisse_prenante : str,
                                         Caisse_prenante: str,
                                         'Région.1': str})

    return mut_data[[Code_Caisse_prenante,
                     Caisse_prenante,
                     Code_Caisse,
                     Caisse,
                     La_mutualisation_est_elle_totale_ou_partielle,
                     Processus,
                     Code_SAS,
                     Theme_de_mutualisation,
                     Typologie,
                     ETP_a_ajouter,
                     ETP_a_deduire,
                     Total_charges_a_ajouter,
                     Total_charges_a_deduire]]


# chargement des données relatives aux mutualisations "classiques"
def process_data(data):
    # extraction du code analytique dans la colonne "Processus"
    data[Processus] = data[Processus].apply(lambda row: row[:4])

    # renommage des colonnes
    data.rename(index=str,
                columns={Code_Caisse_prenante : 'Code organisme prenant',
                         Caisse_prenante : 'Organisme prenant',
                         Code_Caisse : 'Code organisme cédant',
                         Caisse : 'Organisme cédant',
                         La_mutualisation_est_elle_totale_ou_partielle : 'Périmètre',
                         Theme_de_mutualisation : 'Motif',
                         Processus : 'Code analytique'},
                inplace=True)

    # ajout de la colonne "Statut"
    statut_c = data.apply(func=lambda row: np.int(np.isnan(row['Total charges à déduire']) & np.isnan(row['ETP à déduire'])) * "Cédant", axis=1)
    statut_p = data.apply(func=lambda row: (1 - np.int(np.isnan(row['Total charges à déduire']) & np.isnan(row['ETP à déduire']))) * "Prenant", axis=1)
    statut = statut_c + statut_p
    statut.name = "Statut"
    data = pd.concat([data, statut], axis=1)

    # ajout du libellé organisme quand le code n'est pas renseigné (cas de figure : "AUTRES", "AUTRE - CARSAT", "AUTREs - ELSM/DRSM")
    data.loc[data["Code organisme prenant"].isnull(), 'Code organisme prenant'] = data.loc[data["Code organisme prenant"].isnull(), 'Organisme prenant']
    data.loc[data["Code organisme cédant"].isnull(), 'Code organisme cédant'] = data.loc[data["Code organisme cédant"].isnull(), 'Organisme cédant']

    # ajout de la colonne "ETP à déduire/ajouter"
    data['ETP à déduire'] = -1 * data['ETP à déduire']

    data.loc[data['Statut'] == 'Cédant', 'ETP à déduire'] = data.loc[data['Statut'] == 'Cédant', 'ETP à ajouter']
    # ajout de la colonne "Charges à déduire/ajouter"
    data['Charges à déduire'] = data['Total charges à déduire']
    data.loc[data['Statut'] == 'Cédant', 'Charges à déduire'] = data.loc[data['Statut'] == 'Cédant', 'Total charges à ajouter']

    # suppression des colonnes inutiles et ré-ordonnancement des colonnes
    data = data[["Organisme prenant",
                 "Code organisme prenant",
                 "Organisme cédant",
                 "Code organisme cédant",
                 "Statut",
                 "Périmètre",
                 "Code analytique",
                 Code_SAS,
                 "Motif",
                 Typologie,
                 'ETP à déduire',
                 'Charges à déduire']]

    return data

def etl_roam(data_dir=DATA_DIR, sources_dir=SOURCES_DIR, excel_roam_brut=EXCEL_ROAM_BRUT, onglet_mut_clsq=ONGLET_MUT_CLSQ, onglet_mut_phr=ONGLET_MUT_PHR, onglet_mut_nat=ONGLET_MUT_NAT, onglet_deduc=ONGLET_DEDUC):
    mut_clsq_data = process_data(read_xls_mut_tab(onglet_mut=onglet_mut_clsq, data_dir=data_dir, sources_dir=sources_dir, excel_roam_brut=excel_roam_brut))
    mut_phr_data = process_data(read_xls_mut_tab(onglet_mut=onglet_mut_phr, data_dir=data_dir, sources_dir=sources_dir, excel_roam_brut=excel_roam_brut))
    mut_nat_data = process_data(read_xls_mut_tab(onglet_mut=onglet_mut_nat, data_dir=data_dir, sources_dir=sources_dir, excel_roam_brut=excel_roam_brut))
    deduc_data = process_data(read_xls_deduc_tab(onglet_deduc=onglet_deduc, data_dir=data_dir, sources_dir=sources_dir, excel_roam_brut=excel_roam_brut))
    data = pd.concat([mut_clsq_data, mut_phr_data, mut_nat_data, deduc_data], axis=0)
    #     data = add_ETP_ana(data)
    return data

if __name__ == '__main__':
    data = etl_roam()
    writer = pd.ExcelWriter(os.path.join(DATA_DIR,RESULTATS_DIR, 'ROAM_2016.xlsx'))
    data.to_excel(excel_writer=writer, sheet_name='ROAM 2016', index=False)
    writer.save()
