import numpy as np
from config_variables_globales import DATE_INIT

import os

import hashlib

import numpy as np
import pandas as pd

from config_variables_globales import DATA_DIR, EXCEL_TRAM, EXCEL_ROAM, ONGLET_ROAM, ORGANISATION_LIST
from utils import conversion_nat_nan

from config_variables_colonnes import Code_Caisse, Caisse, Region, Processus, \
Code_SAS, Typologie, Theme_de_mutualisation, La_mutualisation_est_elle_totale_ou_partielle, ETP_a_deduire, ETP_a_ajouter, \
Charges_de_personnel_a_deduire, Charges_de_personnel_a_ajouter, Autres_charges_a_deduire, Autres_charges_a_ajouter, \
Total_charges_a_deduire, Total_charges_a_ajouter, Code_Caisse_prenante, Caisse_prenante, Region


from config_variables_colonnes import Caisse_prenante_TRAM, Code_caisse_prenante_TRAM, Caisse_cedante_TRAM, Code_caisse_cédante_TRAM, \
Date_previsionnelle, Date_debut

PROCESSUS_TRAM = ['Gestion de la Paie',
                  'Capitaux Décès',
                  'Invalidité',
                  'Opposition',
                  'Relations Internationales',
                  'FSP LAD',
                  'RCT',
                  'Rente',
                  'PFS Employeurs',
                  'PFS Assurés']

COL_TRAM = ['Code organisme prenant',
            'Organisme prenant',
            'Code organisme cédant',
            'Organisme cédant',
            'Statut',
            'Code analytique',
            Code_SAS,
            'Motif standardisé',
            Typologie,
            Date_previsionnelle,
            Date_debut]

COLONNES_ROAM = ['Code organisme prenant',
                 'Code organisme cédant',
                 'Statut',
                 'Périmètre',
                 'Code analytique',
                 Code_SAS,
                 'Motif',
                 Typologie,
                 'ETP à déduire',
                 'Charges à déduire']

COLONNES_PRE_FINALES = ['Code orga',
                        'Code organisme prenant',
                        'Organisme prenant',
                        'Organisme prenant (TRAM)',
                        'Code organisme cédant',
                        'Organisme cédant',
                        'Organisme cédant (TRAM)',
                        'Statut',
                        'Périmètre',
                        'Code analytique',
                        Code_SAS,
                        'Motif standardisé',
                        'Motif',
                        Typologie,
                        'ETP à déduire',
                        'Charges à déduire',
                        Date_previsionnelle,
                        Date_debut,
                        'join ambigu',
                        'jointure_TRAM',
                        'raccords multiples',
                        'Année',
                        'Code orga annual']

COLONNES_FINALES = ['Code orga',
                    'Code organisme prenant',
                    'Organisme prenant',
                    'Organisme prenant (TRAM)',
                    'Code organisme cédant',
                    'Organisme cédant',
                    'Organisme cédant (TRAM)',
                    'Statut',
                    'Périmètre',
                    'Code analytique',
                    Code_SAS,
                    'Motif standardisé',
                    'Motif',
                    Typologie,
                    'ETP à déduire',
                    'Charges à déduire',
                    Date_previsionnelle,
                    Date_debut,
                    'Date fin',
                    'join ambigu',
                    'jointure_TRAM',
                    'raccords multiples',
                    'doublon',
                    'correspondance(s) Date fin',
                    'MaJ Date fin',
                    'Année',
                    'Code orga annual']

ANNEE_SUIVANTE = '2017-01-01'

HYPOTHESES_DATES = {'Organisme déjà cédant': DATE_INIT,
                    'Pôle individuel': DATE_INIT}

# /!\ a reprendre chaque année /!\ notamment correspondance processus TRAM avec le code analytique
INFOS_TRAM_2016 = {'Relations Internationales': {'code_ana': 1110,
                                                 'code_sas': np.nan,
                                                 'motif standardise': 'Relations Internationales',
                                                 'typo': 'Pôle Régionaux'},
                   'FSP LAD': {'code_ana': 2115,
                               'code_sas': np.nan,
                               'motif standardise': 'FSP LAD',
                               'typo': 'Pôle Régionaux'},
                   'Rente': {'code_ana': 3200,
                             'code_sas': np.nan,
                             'motif standardise': 'Rente',
                             'typo': 'Pôle Régionaux'},
                   'Invalidité': {'code_ana': 3300,
                                  'code_sas': np.nan,
                                  'motif standardise': 'Invalidité',
                                  'typo': 'Pôle Régionaux'},
                   'Capitaux Décès': {'code_ana': 3400,
                                      'code_sas': np.nan,
                                      'motif standardise': 'Capitaux décès',
                                      'typo': 'Centres Nationaux'},
                   'PFS Assurés': {'code_ana': 5120,
                                   'code_sas': np.nan,
                                   'motif standardise': 'PFS Assurés',
                                   'typo': 'Pôle Régionaux'},
                   'PFS Employeurs': {'code_ana': 5120,
                                      'code_sas': np.nan,
                                      'motif standardise': 'PFS Employeurs',
                                      'typo': 'Pôle Régionaux'},
                   'RCT': {'code_ana': 6700,
                           'code_sas': np.nan,
                           'motif standardise': 'RCT',
                           'typo': 'Pôle Régionaux'},
                   'Gestion de la Paie': {'code_ana': 8130,
                                          'code_sas': np.nan,
                                          'motif standardise': 'Gestion de la Paie',
                                          'typo': 'Centres Nationaux'},
                   'Opposition': {'code_ana': 8320,
                                  'code_sas': np.nan,
                                  'motif standardise': 'Opposition',
                                  'typo': 'Pôle Régionaux'}}
